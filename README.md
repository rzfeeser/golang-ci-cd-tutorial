# GoLang CI-CD Tutorial

## Introduction

This repository is to experiment with the Go programming langauge and GitLab's CI / CD pipelines. The `main` branch does not have a `.gitlab-ci.yml` by design. This is so you can easily create a branch and run you own experimental `.gitlab-ci.yml` file. Two (2) other branches exist; `ci-go` and `ci-config`.
- `ci-config` - This branch has a `.gitlab-ci.yml` file that while working, does nothing functional relating to CI/CD. This branch is just for experimenting.
- `ci-go` - This branch has a `.gitlab-ci.yml` file that contains a functional CI/CD pipeline. When executed it will first test the code within the repository, compile, and then run the resultant applciation.


## Getting Started

The `.gitlab-ci.yml` file in this repository will cause automation to run tests against the code within. If tests are passed, the application will be run as well.


## Resources
- [The Go Programming Language](https://go.dev/)
- [GitLab - Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)
- [GitLab - .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
- [GitLab - .gitlab-ci.yml Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)


## Author
@RZFeeser - Author & Instructor - Feel free to reach out to if you're looking for a instructor led training solution.
